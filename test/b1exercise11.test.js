var assert          = require('chai').assert
var celsius         = require('../02_javascript/block1/exercise11').celsius
var fahrenheit      = require('../02_javascript/block1/exercise11').fahrenheit
var toCelsius       = require('../02_javascript/block1/exercise11').toCelsius
var toFahr          = require('../02_javascript/block1/exercise11').toFahr;


describe("#test1", function(){
    it ('fahrenheit should be a number', function(){
        assert.typeOf(fahrenheit, 'number')
    })
})
describe("#test2", function(){
    it ('celsius should be a number', function(){
        assert.typeOf(celsius, 'number')
    })
})
describe("#test5", function(){
    it ('toCelsius should be a function', function(){
        assert.typeOf(toCelsius, 'function')
    })
})
describe("#test6", function(){
    it ('toFahr should be a function', function(){
        assert.typeOf(toFahr, 'function')
    })
})
describe("#test7", function(){
    it (`toFahr should be 102`, function(){
        assert.equal(toFahr(39),102)
    })
})
describe("#test8", function(){
    it (`toCelsius should be 39`, function(){
        assert.equal(toCelsius(102),39)
    })
})
describe("#test9", function(){
    it (`toFahr should be 68`, function(){
        assert.equal(toFahr(20),68)
    })
})
describe("#test10", function(){
    it (`toCelsius should be 57`, function(){
        assert.equal(toCelsius(134),57)
    })
})
