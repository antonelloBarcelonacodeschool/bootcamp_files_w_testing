var assert       = require('chai').assert
var isEven      = require('../02_javascript/block1/exercise7').isEven;


describe("#test1", function(){
    it ('isEven should be a function', function(){
        assert.typeOf(isEven, 'function')
    })
})
describe("#test3", function(){
    it ('isEven(7) should be false', function(){
        assert.equal(isEven(7),false)
    })
})
describe("#test4", function(){
    it ('isEven(6) should be true', function(){
        assert.equal(isEven(6), true)
    })
})
describe("#test5", function(){
    it ('isEven(9) should be false', function(){
        assert.equal(isEven(9), false)
    })
})