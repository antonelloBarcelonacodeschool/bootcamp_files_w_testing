const age = 35, end_age = 70, teas_day = 3;
const howManyTeas = (age, end_age, teas_day) => (end_age - age) * (365 * teas_day);
module.exports = { howManyTeas, age, end_age, teas_day};
//  == HINT == 
    // Subtract the age from the end_age and multiply the result for the number of teas per day, per number of days.
//  == STEPS == 

// Create a variable called age and assign it a value of 35
// Create a variable called end_age and assign it a value of 70
// Create a variable called teas_day and assign it a value of 3
// Create a function called howManyTeas which takes our 3 variables as arguments.
// Subtract the age from the end_age and multiply the result for the number of teas per year per number of days.
