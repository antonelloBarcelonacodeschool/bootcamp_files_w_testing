const now = 2018, dob = 1982;
const howManyDays = (dob, now) => `you have lived for ${(now - dob) * 365 } days already!`;
module.exports = { now, dob, howManyDays };
//  == HINT == 
// subtract the dob from the current year and then convert it in days.
//  == STEPS == 
// declare two variables, the current_year and the year_of_birth
// declare a function called howManyDays which takes these two variables as arguments
// subtract the year of birth from the current year and multiply the result time 365
// return the result in a sentence as described exactly replacing *'DAYS'* with the above result.
//
