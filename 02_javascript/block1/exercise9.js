const name = "Jason", birth_year = 1982, now = 2018;
const getAge  = (birth_year, now, name) =>
    `Hello ${name} you are ${now - birth_year} years old`   
module.exports = { name, birth_year, now, getAge };
//  == HINT == 
    // you can use template strings to easily inject variables in a string to have the expected output.
//  == STEPS == 
    //  Define 3 variables: name, birth_year and now, giving them the values of your name, your birth year and the current year.
    //  Define a function called getAge which takes our 3 variables as arguments;
    //  inside it create a variable called age, its value should be result of subtracting birth_year from now.
    // inject the name coming as argument and age in the sentence as shown in the example.