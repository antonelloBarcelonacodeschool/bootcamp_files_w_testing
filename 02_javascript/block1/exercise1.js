const apple  = 5, apple2 = 15;
const total  = (apple, apple2) => apple + apple2;
module.exports = { apple, apple2, total };

//  == HINT == 
// Return the sum of apple and  apple2
//  == STEPS == 
// declare a variable called apple and assign it a value of 5
// declare a variable called apple2 and assign it a value of 15;
// Pass apple and apple2 as arguments to the function
// Return their sum;

