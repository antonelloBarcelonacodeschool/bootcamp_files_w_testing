const a = 3, isEven = a => a % 2 === 0;
module.exports = { isEven };
//  == HINT == 
//  == HINT == 
    // You don't need to use conditionals statements for this exercise ...
//  == STEPS == 
// define a function called isEven, which takes one argument, a
// in the return use modulus operator to check if the passed argument is divisible by 2 without remainder
// if done correctly is will return true if it is and false if it is not