const celsius = 35, fahrenheit = 120;
const toFahr = c => Math.round(c * 9 / 5 + 32);
const toCelsius = f => Math.round((f -32) * 5 / 9);
module.exports = { celsius,  fahrenheit, toCelsius,  toFahr };
// == HINT ==
//If the result seems correct but it doesn't pass the test you could try rounding the result with Math.round().
// == STEPS ==
// Store Celsius and Fahrenheit temperature into 2 variables.
// declare a function called toCelsius which takes 1 argument – the temperature in Fahrenheit, 
// declare a function called toFahr which takes 1 argument – the temperature in Celsius,
// inside each function return the result of the applied formula to convert using the function argument as the value.