const date_of_birth = 1982, future_year = 2018;
const ageCalc = (date_of_birth, future_year) => future_year - date_of_birth;
module.exports = { ageCalc, date_of_birth, future_year };
//  == HINT == 
// Return the result of subtracting dob from the future year. 

//  == STEPS == 
// Create a variable called date_of_birth
// Create a variable called future_year
// Create a function called ageCalc
// Pass to it the 2 variables you just created as arguments
// Return the result of subtracting dob from the future year. 
