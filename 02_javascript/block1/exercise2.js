const a  = 5, b = 15;
const multy = (a, b) => a * b;
module.exports = { a, b, multy };
//  == HINT == 
// Return the result of the multiplication of a and  b.
//  == STEPS == 
// Declare a variable called a and assign it a value of 5
// Declare a variable called b and assign it a value of 15;
// Pass a and b as arguments to the function
// Return the result of their multiplication;

