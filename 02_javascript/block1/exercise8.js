const age = 21, minAge  = 15;
const checkAge = (age, minAge) => age >= minAge;
module.exports = { checkAge, age, minAge };
//  == HINT == 
// evaluate if age is bigger or equal than minAge.
//  == STEPS == 
// declare a variable called age and assign it a value of 21
// declare a variable called minAge and assign it a value of 15
// evaluate if age is bigger or equal than in age.
// return the result of the evaluation.