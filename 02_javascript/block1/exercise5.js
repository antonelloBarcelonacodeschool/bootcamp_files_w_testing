const compare = (a, b) => a > b;
module.exports = { compare };
//  == HINT == 
// You don't need to use conditionals statements for this exercise ...
//  == STEPS == 
// Define a function called compare, which takes two arguments, a and b, 
// Return the result of comparing them with each other using the greater than sign.