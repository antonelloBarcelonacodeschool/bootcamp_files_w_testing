const compare = (a,b) => a === b;
module.exports = { compare };
//  == HINT == 
    // You don't need to use conditionals statements for this exercise ...
//  == STEPS == 
// define a function called compare, which takes two arguments, a and b, 
// and returns the result of comparing them with each other using strict equality operator.