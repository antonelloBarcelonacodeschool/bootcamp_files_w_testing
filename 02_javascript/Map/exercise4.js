module.exports = function (maxBudget, gifts){
	var total =0, giftsPurchased = new Map();
	for (let [key, val] of gifts){
		 if(total <= maxBudget && total + val <= maxBudget) {
			giftsPurchased.set(key, val);
			total += val;	
         }
    }
    giftsPurchased.set('total', total);
	return giftsPurchased;
}