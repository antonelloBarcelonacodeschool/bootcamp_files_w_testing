const getIndex = (a, key, val) => {
	let ind = -1;
	for (let i = 0; i < a.length; i++){
	    if(a[i][key] === val) {
			ind = i;
			break;
		}
	}
	return ind
}
module.exports ={ getIndex };

// == HINT === 
// A 'for' loop is ideal for this.
// == STEPS ===
// Declare a function  called 'getIndex' which takes 3 arguments: an array, a key and a value.
// Declare a variable called 'index' and give it a value of -1.
// Using a 'for' loop, loop through the array and for each iteration check if the element at the passed key has the passed value.
// If you find a match set 'index' to the current index in the loop and break from the loop using the keyword 'break';
// Outside the loop return 'index'.