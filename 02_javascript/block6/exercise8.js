const last = obj => {
    for (var key in obj){}
    return {[key]:obj[key]};
}
module.exports ={ last };
// == HINT === 
// No methods are needed for this exercise, just a simple 'for in' loop! Check the last iterator value after the loop ends and use it!
// == STEPS ===
// Create a function called 'last' which takes an object as argument.
// Inside it loop through the object (argument).
// You don't need to do anything in the loop
// Then outside the loop create an empty object and using the iterator of the 'for in' loop grab the key and value and assign them to this newly created object.
// They will hold the the last values because of the way the loop works.