const swap = obj =>{
    let newObj = {};
    for (var key in obj){
        newObj[obj[key]] = key
    }
    return newObj;
}
module.exports= { swap };

// == HINT === 
    // whatever is in the "[]" before the  "=" is the key, whatever is after the "=" is the value ...
// == STEPS ===
// create a function called 'swap' which takes an object as argument.
// inside it define a new object called 'newObj'.
// loop inside the object (argument)
// for each iteration 
    //add the object (argument) key as the 'newObj' value 
    //add the the object (argument) value as the 'newObj' key
// outside the loop return 'newObj'