const addToObj = (key,val)=>{[key]:val};
module.exports = { addToObj };
// == HINT === 
// Use square bracket notation for the key!
// == STEPS ===
// Define a function called 'addToObj' which takes two arguments: a key and a value.
// Using square bracket notation return a new object adding the key as key and the value as the value.
