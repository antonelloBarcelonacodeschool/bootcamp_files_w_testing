let obj = {a: 1, b: 2}
const modifyObject = (obj, key, val) =>{
    obj[key] = val;
    return obj
}
module.exports ={ obj, modifyObject };

// == HINT === 
// Use square brackets notation...
// == STEPS ===
// Define a function called 'modifyObject' which takes 3 arguments: an object, a key and a value.
// Inside it add the new key and value to the object
// Return the object.