const model = (action, obj, schema) => {
    const DB = []
	if (!action || action !== "add") return "missing action argument or wrong provided";
    let newObj= {}
    for (let key in obj) {
        schema.includes(key) ? newObj[key] = obj[key] : null
    }
    DB.push(newObj)
	return DB;
}
module.exports = { model };
// == HINT === 
// Only use the 'DB' array at the exercise's end.
// == STEPS ===
// Define a function called model which takes 3 arguments: an action, an object and a schema.
// Inside it create an empty array called 'DB'.
// // First make sure that the argument action is present and that its value is "add" 
    // If it's not then you should return the following error "missing action argument or wrong provided"
// If it is then declare an empty object called 'newObj'.
// Then loop through the obj (argument) using a 'for in' loop.
// For each iteration check if the key of the object is present in the schema
    // If it is then add the key and value to our 'newObj'.
    // Then outside the loop push our 'newObj' to the 'DB' array.
    // Return the array.