// const model = (action, obj, schema) =>{
//     const DB = []
//     if (!action || action != 'add') return "missing action argument or wrong provided";
//     let newObj = {}
//     for(let key in obj){
//         key in schema && typeof obj[key] === schema[key].type ? newObj[key] = obj[key] :null
//     }
//     for (let key in schema){
//         newObj[key] === undefined && schema[key].default !== undefined ? newObj[key] = schema[key].default : null
//     }
//     DB.push(newObj)
//     return DB;
// }

let model = (add, obj, schema) => {
    var DB = []
    var result = {};
    
    if(add != 'add') {return null}
    for(var key in schema){

            if (typeof obj[key] == schema[key].type) {
            result[key] = obj[key];
            } else if('default' in schema[key]) {
            result[key] = schema[key].default
            }
    }

    DB.push(result)
    return DB
}





module.exports = {
    model
}
// == HINT === 
// You may want to consider using the "in" operator to check if an object contains a key.
// == STEPS ===
// Define a function called 'model' which takes 3 arguments: an action, an object and a schema.
// Inside it create an empty array called 'DB'.
// // First make sure that the argument action is present and that its value is "add" 
    // If it's not then you should return the following error "missing action argument or wrong provided"
// If it is then declare an empty object called 'newObj'.
// Then loop through the obj (argument) using a 'for in' loop.
// For each iteration check if the key of the object is present in the schema and if the value of the object is of the same typeof of the value in the schema.
    // If it is then add the key and value to our 'newObj'.

// Then loop through the schema and for each iteration check if the value is not present in the 'newObj' and that is present in the default.
    // In this case add the default to the 'newObj'.
    // Then outside the loop push our 'newObj' to the 'DB' array.
    // Return the array.