const splice = (obj, start, end) =>{
	let newObj = {}, count = 0;
	for(let key in obj){
		
		count <= end && count >= start ? newObj[key] = obj[key] : null
		start === end ? 
		count-1 === end ? newObj[key] = obj[key] : null
		: null
		count ++;
	}
	return newObj;
}
module.exports ={
	splice
}
// == HINT === 
// You can use a counter in place of the index.
// == STEPS ===
// Define a function called 'splice' which takes 3 arguments: an object, a start and an end.
// Inside it define a new empty object called 'newObj' and a variable called 'count' with a value of 0.
// Loop through the object(argument) and for each iteration
	// increase the value of 'count' by one.
	// check if 'count' is less than or equal to the end and 'count' is bigger than start.
		// if that's the case then assign the key, value of the object to 'newObj'.
	// If start is equal to end 
		// if 'count' is also equal to end assign the key, value of the object to 'newObj'.
// Then outside the loop return the 'newObj'.
