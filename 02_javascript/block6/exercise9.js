const sumAll = obj => {
    if(!obj)return 0;
    if(obj &&  Object.values(obj).length > 0)return Object.values(obj).reduce((a,b)=>a+b);
    return Object.values(obj)[0];
}
module.exports = { sumAll };

// == HINT === 
// You could use Object.values to get all values easily.
// == STEPS ===
// Define a function called 'sumAll' which takes an object as argument.
// Then do some checks:
    // if the object is empty return 0;
    // if the object has only one value then return that.
    // if the object has more then you can loop and sum them.
    // return the sum.