// const sort = obj => {
//     const arr = [];
//     for (let key in obj) {
//         arr.push([key, obj[key]]);
//     }
//     arr.sort((a, b)=>  a[1] - b[1]);
//     let newObj = {}
//     arr.forEach(ele => newObj[ele[0]] = ele[1])
//     return newObj;
// }

var sort = function(obj){
var newObj = {}
var arr = Object.values(obj).sort(function(a,b){
return a-b 
})
arr.forEach(function(ele, index){
	newObj[index] = ele
})
return newObj
}

module.exports ={
    sort
}
// == HINT === 
// Objects can't be sorted ...
// == STEPS ===
// define a function called 'sort' which takes an object as argument.
// inside it declare an empty array called 'arr'.
// loop inside the object and for each iteration push an array which contains the key and the value to 'arr'.
// Sort the array numerically by the object value (position one in our nested arrays)
// create an empty object called 'newObj'.
// Loop through the array and for each iteration assign the first and second element as the key and value respectively to our 'newobJ'
// return the 'newObj'.