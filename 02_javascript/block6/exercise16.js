const sort = (obj, by, order) => {
    let sortAction = order+by
    let arr = Object.entries(obj)

    let sorter = {
        ascendingvalues  : ( ) => arr.sort((a, b) =>  a[1] - b[1]),
        descendingvalues : ( ) => arr.sort((a, b) =>  b[1] - a[1]),
        ascendingkeys    : ( ) => arr.sort(),
        descendingkeys   : ( ) => arr.sort().reverse()
    }
    sorter[sortAction]()
    let newObj = {}
    arr.forEach(ele => newObj[ele[0]] = ele[1])
    return newObj;
}

module.exports ={
    sort
}
// == HINT === 
// Consider using an object to hold the methods for different sorting.
// == STEPS ===
// Create a function called 'sort' which takes 3 arguments: an object, a sortBy (key || value) and a sort_order (ascending || descending)
// Create a variable called 'sortAction', concatenate 'sortBy' and 'sort_order' and set that as the value of 'sortAction'.
// inside it declare an empty array called 'arr'.
// loop inside the object and for each iteration push an array which contains the key and the value to 'arr'.
// Define an object called 'sorter' and add to it 4 methods.
    // 'ascendingvalues' : Sorts the array numerically (ascending order) by the object value (position one in our nested arrays);
    // 'descendingvalues': Sorts the array numerically (descending order) by the object value (position one in our nested arrays);
    // 'ascendingkeys' : sort by using the plain Array.prototype.sort.
    // 'descendingkeys' : sort by using the plain Array.prototype.sort then reverse it.
// Call the needed method using the 'sortAction' variable.
// create an empty object called 'newObj'.
// Loop through the array and for each iteration assign the first and second element as the key and value respectively to our 'newObJ'
// return the 'newObj'.
