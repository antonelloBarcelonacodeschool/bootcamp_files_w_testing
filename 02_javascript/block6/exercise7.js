const runOnRange = obj => {
    const arr = [];
    let { start, end, step } = obj;
	if (!step) return arr;
	if (start > end && step < 1) {
	 	for (let i = start; i >= end; i+=step){
        	arr.push(i);
    	}
    }
    for (let i = start; i <= end; i+=step){
        arr.push(i);
    }
    return arr;
}

module.exports = { runOnRange };

// == HINT === 
// Consider using a standard 'for' loop here...
// == STEPS ===
// Define a function called 'runOnRange' which takes an object as argument.
// Inside it define an empty array called 'arr'.
// Assign start, end, step each to a variable, taken from the object, passed as the argument.
// If 'step' is missing return 'arr'.
// Create two 'for' loops, one that runs if 'start' is greater than 'end' and 'step' is less than 1.
    // This loop will start at 'start', and run until the iterator is greater or equal to 'end'.
// The other loop will run if the condition isn't met, and will start at 'start' but run until the iterator is less or equal to the 'end'.

// In either of the loops you should push the value of the iterator to the array for each iteration.
// Outside the loops return 'arr'.