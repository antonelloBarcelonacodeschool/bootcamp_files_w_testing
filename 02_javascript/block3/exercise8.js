const timesTwo = arr => String(arr.map(ele => ele * 2));
module.exports ={
    timesTwo
}
// == HINT === 
// Loop and multiply each element by 2
// == STEPS ===
// Create a function called timesTwo which takes an array as argument.
// Inside it define an empty array.
// Loop through the array (the argument) and multiply each element by two while pushing it to the empty array we defined.
// Then outside the loop return the formerly empty array.