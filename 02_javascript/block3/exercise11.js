const lowerCaseLetters = (str)=> {
    let arr =[];
    str.split('').map(ele => {
		if(isNaN(Number(ele))) {
			 ele == ele.toUpperCase()  ? arr.push(' '+ele) : arr.push(ele);
		}
	})
    return arr.join('').toLowerCase().trim();
}

module.exports = { lowerCaseLetters };
// == HINT === 
// We have an upper case at the beginning of each word.
// == STEPS ===
// Define a function called lowerCaseLetters which takes a string as argument.
// Inside it define an empty array.
// Loop through the string using the loop of your choice.
// First check if the element is not and cannot be converted to a number using isNaN.
// Then check, if the character is uppercase then push it with a space before it, if is lowercase push it without it.
// Then outside the loop trim (String.prototype.trim) the string to get rid of unwanted spaces.

// == LOGIC ===

// Start a function, takes a string as an argument

// 	declare an empty array to hold the data 

// 	start looping on a string
// 		for the current character check if it is not a number
// 			check if it's uppercasse when push to array with a space before
// 			else if lowercase push to array as it is
// 	end looping

// 	join array into string, 
// 	make everything lowercase, 
// 	remove spaces around

// 	return resulting string
// end function