const reverser = str => {
    let arr= []
    for (let i = str.length; i>=0; i--){
        arr.push(str[i]);
    }
    return arr.join('');
}
module.exports = { reverser };
// == HINT === 
// Use a for loop here and decrease the iterator's value to 0!
// == STEPS ===
// Declare a function called reverser which takes a string as argument (str).
// Inside it declare an empty array,
// Using a for loop set the i to be equal to the string length and loop until i is greater or equal than 0, decreasing i's value by one each time the loop runs.
// Inside the loop push each element of str to our empty array.
// Join our array to make it a string and return it.