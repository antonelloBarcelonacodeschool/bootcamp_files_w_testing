const shortener = str => {
	let word = str.split(' ');
	return `${word[0].replace(word[0][0],word[0][0].toUpperCase())} ${word[1][0].toUpperCase()}.` 
}
module.exports = {
    shortener
}
// == HINT === 
 //String.prototype.split can be useful to distinguish 2 words in the argument... 

// == STEPS ===
// Define a function called shortener which takes a string as an argument.
// Split the string in 2 words passing the empty space as argument.
// Create a new string which has to:
    // Get the first word, 
    // The first letter of the second word made upperCase and a dot.
    // Concatenate them and return them.
