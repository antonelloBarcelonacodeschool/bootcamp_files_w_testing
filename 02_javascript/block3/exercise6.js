const sum = arr => arr.reduce((a,b)=>a+b)
module.exports ={
    sum
}
// == HINT === 
// You can do this with any loop.
// == STEPS ===
// Create a function called sum which takes an array as argument.
// Inside it create a variable called total and assign it a value of 0.
// Then loop through the array and add each number from the array to the variable total
// Then outside the loop return total
