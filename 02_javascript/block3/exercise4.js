const arr = ['one','two','three','four']
const looper = arr =>{
    let count = 0;
    arr.forEach((ele, i)=>{
        count++
        console.log(ele, i)
    })
    return count
}
module.exports = { 
    looper, arr
}
// == HINT === 
// Just a simple forEach loop
// == STEPS ===
// Define an array as shown in the example.
// Create a function called looper which takes our array as its solo argument.
// Inside it define a variable called count and give it a value of 0.
// Loop through the array and each time the loop runs increase the value of i by one.
// Still inside the loop console.log the first and second argument of the forEach loop
// Outside the loop return count
