const isEven = arr =>{
    let count = 0;
    arr.forEach(ele => ele % 2 === 0 ? count++ : null)
	return count;
}

module.exports ={
    isEven
}
// == HINT === 
// Use the modulus operator – %
// == STEPS ===
// Create a function called isEven which takes one argument, an array.
// Inside it declare a variable called count and give it a value of 0.
// Loop through the array and each time you find a number that is even increase the value of count by one.
// Outside the loop return count.
