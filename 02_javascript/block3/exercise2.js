let i = 11;
const firstLoopReverse = () => {
    for (i; i >= 1 ; i-- ){
        console.log(i)
    }
return i
}
module.exports = {
    firstLoopReverse, i
}
// == HINT === 
// Declare the variable i outside the function.
// == STEPS ===
// Declare a variable i with a value of 11.
// Create a function called firstLoop
// Create a loop that goes until i is greater than or equal to 1
// In the loop decrease the value of i by one each time the loop runs.
// Outside the loop return i.
