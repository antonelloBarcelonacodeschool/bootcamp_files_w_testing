const twoArrays = (arr, arr2)=>{
    let count =0;
    arr.forEach((el, i) => el === arr2[i] && count++)
    return count;
}
module.exports ={
    twoArrays
}
// == HINT === 
// You can loop through multiple arrays of the same length with just one loop
// == STEPS ===
// Declare a function called twoArrays which takes 2 arguments (arr, arr2)
// Inside it declare a variable called count and give it a value of 1.
// Loop through arr with a forEch loop, you can use the second argument (the index) to access arr2.
// Once you have access to both arrays you can compare each element in the loop 
// And every time you find a match increase the value of count by one.
// Then outside the loop return count.
