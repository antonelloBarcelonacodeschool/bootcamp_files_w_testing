const isString = arr =>{
    const strings = []
    arr.forEach( ele => typeof ele == "string" ? strings.push(ele) : null);
    return strings;
}
module.exports = {
    isString
}
// == HINT === 
// You should use typeof to find all the strings
// == STEPS ===
// Create a function called isString which takes an array as argument.
// Inside it define a new array (empty)
// Loop through the old array and check each element of the array to see if it is of the type 'string'.
// If it is push it to the empty array we defined outside the loop.
// Then outside the loop return the array containing the strings we pushed.