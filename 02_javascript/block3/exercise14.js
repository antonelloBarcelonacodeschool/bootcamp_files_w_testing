const budgetTracker = arr =>
    Math.round((arr.reduce((a,b) => Number(a)+Number(b)) / arr.length) * 0.0089);

module.exports = {
    budgetTracker
}
// == HINT === 
// Make sure all the elements of the sum are numbers ...
// == STEPS ===
// Define a function called budgetTracker which takes an array as argument.
// Inside it create a variable called total.
// Using a loop convert each element to a number and add it to the variable total.
// Divide the total by the number of days
// Multiply the result for the exchange rate
// Return it.