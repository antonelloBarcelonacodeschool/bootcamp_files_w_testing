const multy = arr => arr.reduce((a,b)=>a*b)
module.exports ={
    multy
}
// == HINT === 
// You can do this with any loop.
// == STEPS ===
// Create a function called sum which takes an array as argument.
// Inside it create a variable called total and assign it a value of 1.
// Then loop through the array and multiply each number from the array to the variable total and assign the results of multiplication back to the total itself.
// Then outside the loop return total
