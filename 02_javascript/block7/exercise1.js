const moviesDB = (DB, genre, movie)=>{
    const index = DB.findIndex(a => a.genre == genre);
    if(index !== -1){
		const movieIndex = DB[index].movies.findIndex(a => a.title === movie.title);
		if (movieIndex === -1 ){
            DB[index].movies.push(movie) 
        }
        else {
            return `the movie ${movie.title} is already in the database!`
        }
    }else {
        DB.push({ genre:genre, movies:[movie] })
    }
    return DB
}

module.exports ={
    moviesDB
}

// == HINT === 
// Use Array.prototype.findIndex to make your life easier.
// == STEPS ===
// Declare a function called 'moviesDB' which takes 3 arguments: an array(DB), a genre and a movie.
// Use Array.prototype.findIndex to check if the genre is present in the array.
    // If it's not:
        // Push the an object with the genre and an array called movies which contain the movie(argument).
    // If it is present:
        // At the index of the genre, Use Array.prototype.findIndex to check if the movie is there:
            // If is not there:
                // push it in at that index.
            // If is there already:
                // return the movie ${THE TITLE OF THE MOVIE} is already in the database!`
// at the end return DB.