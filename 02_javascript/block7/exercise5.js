const shuffle  = a => {
	a.forEach( (_, i) => {
		let newPos = Math.floor(Math.random() * (i + 1)), temp= a[newPos]
		a[newPos] = a[i], a[i] = temp
	})
	return a 
}
module.exports = {
    shuffle
}
// Array.prototype.randomInRange = function () {
//   return Math.floor(Math.random() * (this[1] - this[0]) + this[0]);
// }
// Array.prototype.shuffle = function () {
// 	var newArr  = [], length = this.length;
// 	for (var i = 0; i < length; i++){
// 		var random = [0, this.length-1].randomInRange();
// 		if(newArr.length < length) newArr.push(...this.splice(random, 1));

// 	}
// 	return newArr;
// }

// == HINT === 
// Use Math.random to generate a random number and Math.floor to round it!
// == STEPS ===
// Create a function called 'shuffle' which takes an array as argument.
// Inside it loop through the array.
	// For each iteration generate a random number using Math.random and swap the element at the current index with one at the random index.
//return the array.