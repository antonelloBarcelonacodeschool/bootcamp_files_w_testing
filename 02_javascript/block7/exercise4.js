class Universe {
    constructor(init, which ) {
       var self  = this
        this.matter = {
            total: which == 'matter' ? init : 0,
            destroy:function(val){
                this.total -= val
                self.energy.total += val
            },
            create:function(val){
                this.total += val
                self.energy.total -= val
            }
        }
        this.energy = {
            total: which == 'energy' ? init : 0,
            destroy:function(val){
                this.total -= val
                self.matter.total += val
            },
            create:function(val){
                this.total += val
                self.matter.total -= val
            }
        }
    }
} 


// with arrow functions:

// class Universe {
//     constructor(init, which ) {
//         this.matter = {
//             total: which == 'matter' ? init : 0,
//             destroy:(val)=>{
//                 this.matter.total -= val
//                 this.energy.total += val
//             },
//             create:(val)=>{
//                 this.matter.total += val
//                 this.energy.total -= val
//             }
//         }
//         this.energy = {
//             total: which == 'energy' ? init : 0,
//             destroy:(val)=>{
//                 this.energy.total -= val
//                 this.matter.total += val
//             },
//             create:(val)=>{
//                 this.energy.total += val
//                 this.matter.total -= val
//             }
//         }
//     }
// } 

module.exports = {
    Universe
}

// == HINT === 
// You could assign this to a variable to access it where is not available.
// == STEPS ===
// Create a constructor function called 'Universe' which takes two arguments: an initial value and the destination object (matter or energy).
// Inside declare a variable called 'self' and give it this as the value.
// Using the keyword "this" create an object called 'matter':
    // in it give it a property 'total' and give it a value of or either the initial value passed as argument or 0, depending on weather the destination object was 'matter' or 'energy'.
    // Create a method called 'destroy' which takes one argument, a value, and subtracts it from the 'total' of 'matter' and adds it in the same time to the 'total' of 'energy'.
    // Create a method called 'create' which takes one argument, a value, and adds it to the 'total' of 'matter' and subtracts it from the 'total' of 'energy'
// Using the keyword "this" create an object called 'energy':
    // in it give it a property 'total' and give it a value of or either the initial value passed as argument or 0, depending on weather the destination object was 'matter' or 'energy'.
    // Create a method called 'destroy' which takes one argument, a value, and subtract it form the 'total' of 'energy' and adds it on the 'total' of 'matter'.
    // Create a method called 'create' which takes one argument, a value, and adds it to the 'total' of 'energy' and subtracts it from the 'total' of 'matter'
