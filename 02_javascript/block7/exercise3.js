class BankAccount {
	constructor(amount){
		this.amount = amount  || 0;
	}
    deposit(val) { this.amount +=val  }
    withdraw(val) { this.amount -= val }
    balance(){return this.amount}
}
module.exports = {
    BankAccount
}
// == HINT === 
// You may need a default value for the amount
// == STEPS ===
// Define a class called 'BankAccount'
// The constructor takes one argument: the amount.
// If the amount is not present it should default to 0.
// Create a method called 'deposit' which takes one argument; a value, it will then add this to the amount.
// Create a method called 'withdraw' which takes one argument; a value, it will then subtract this from the amount.
// Create a method called 'balance' which takes no arguments and simply returns the amount.
