let addCurrency = (coin, value, crypt) => {
    const index = crypt.findIndex(a => a.coin == coin.coin);
    let myCoin = coin.coin
        const moneta = myCoin.substring(0,1).toUpperCase() + myCoin.substring(1,myCoin.length);
    if(index === -1) {
        crypt.push(coin) 
        
        return `New coin ${moneta} added to Database`;
    }
    else return findCurrency(moneta, value, index, crypt);
}

let findCurrency = (moneta, value, index, crypt) => {
    const rate = crypt[index].rate 
    return converter(value, rate, moneta)
    
}

let converter = (value, rate, moneta) => {
    let result;
    rate > 0  ? result = value * rate : rate = value / rate;
    return tellConversion(result, moneta, value)
}

let tellConversion = (result, moneta, value) => `You will receive ${result} usd for your ${value} ${moneta}s`;


module.exports = {
    addCurrency, findCurrency, converter, tellConversion
}





// == HINT === 
// Use Array.prototype.findIndex to make your life easier.
// == STEPS ===
// Define a function called 'addCurrency' which takes 3 arguments: a 'coin' a 'value' and an array ('crypt')
// Inside use Array.prototype.findIndex to see if the 'coin' is already present in the 'crypt' array. storing the index if you find it.
// if it is NOT:
    // push it to the 'crypt' array.
    // then return a string as the one below replacing the word COIN with the name of the 'coin' you added (capitalize).
// If it is there, call 'findCurrency' to get the rate.
// Define a function called 'findCurrency' which takes 3 arguments: a 'coin' a 'value' an index (from 'addCurrency') and the 'crypt' array.
    // get the rate of the 'coin' using the index to access the right 'coin' from the 'crypt' array.
    // call the converter function passing the 'value', the rate and the 'coin'.
// Define a function called converter which takes 3 arguments, a 'value' a rate and a 'coin'.
    // Perform the conversion and return call tellConversion to output the result.
// Define a function called tellConversion which takes 3 arguments a result, a 'coin' and a 'value'.
// Create a string as shown in the example using the arguments of tellConversion.

