var familyAffairs = (perfectFamily,otherFamilies) => {
    let noMom, noKids, withKids, age = 0;
    for( let i = 0; i < otherFamilies.length; i++ ){ 
         for( let key in otherFamilies[i] ){
            if( !('mother' in otherFamilies[i][key])){
               noMom = key
               break 
            }
            if( !('son' in otherFamilies[i][key]) && !('daughter' in otherFamilies[i][key]) ){
               noKids = key 
            }
            if( 'son' in otherFamilies[i][key] && otherFamilies[i][key].son.age > age){
                 age = otherFamilies[i][key].son.age
                 withKids = key 
            }
            if( 'daughter' in otherFamilies[i][key] && otherFamilies[i][key].daughter.age > age){
                 age = otherFamilies[i][key].daughter.age
                 withKids = key 
            }
            // if( noMom ) { break }
         }
    }
    return `Yay! ${perfectFamily.mother.name} moved to ${noMom || noKids || withKids}`
}

// let familyAffairs = (ff, fams)=>{
// 	let mom = ff.mother
// 	let oldestKid = {age:0, fam: -1}
// 	for (var i = 0;i<fams.length; i++) {
// 		for (var lastname in fams[i]){
// 			console.log(!('mother' in fams[i][lastname]))
// 			if ( !('mother' in fams[i][lastname]) ) {
// 				fams[i][lastname].mother = mom
				
// 				return `Yay! ${mom.name} moved to ${Object.keys(fams[i])}` 
// 			} 
// 		}
// 	}

// 	for (var i = 0;i<fams.length; i++) {
// 		for (var lastname in fams[i]){

// 			if ( !('son' in fams[i][lastname]) && !('daughter' in fams[i][lastname]) ){ 
// 				fams[i][lastname].mother = mom
				
// 				return `Yay! ${mom.name} moved to ${Object.keys(fams[i])}` 
// 			} else {
				
// 				if(fams[i][lastname].son){ 
// 					if(fams[i][lastname].son.age > oldestKid.age ) {
// 						oldestKid.age = fams[i][lastname].son.age
// 						oldestKid.fam = i
// 						oldestKid.lastname = lastname
// 					}
// 				}else if (fams[i][lastname].daughter) {
// 					if(fams[i][lastname].daughter.age > oldestKid.age) { 
// 						oldestKid.age = fams[i][lastname].daughter.age
// 						oldestKid.fam = i
// 						oldestKid.lastname = lastname
// 					}
// 				} 
// 			}

// 		}
// 	}
// 	fams[oldestKid.fam][oldestKid.lastname].mother = mom
// 	delete ff.mother
// 	return `Yay! ${mom.name} moved to ${Object.keys(fams[oldestKid.fam])}` 
// }



module.exports = {
	familyAffairs
}
