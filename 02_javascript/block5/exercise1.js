const calc = (a, b , opt) => eval(a + opt + b) ?  eval(a + opt + b) : 'wrong data provided'

module.exports = {
    calc
}
// == HINT === 
// You might want to take a look at 'eval' method to make your code shorter ...
// == STEPS ===
// Define a function called 'calc' which takes 3 arguments.
// Set up a condition for each of the possible 4 signs (+, -, *, /);
// Before running any of these check that all arguments are there, if any is missing return 'wrong data provided'
// Otherwise do the proper operation based on the sign.
// Return the result.