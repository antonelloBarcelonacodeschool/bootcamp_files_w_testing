const tellAge = (m, d, y) =>{
    let timeDiff = new Date() - new Date(`${m}/${d}/${y}`);
    if ( timeDiff < 1) return "You can't be born in the future!"
    let days = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
    days > 365 ? days = `You are ${parseInt(days/365)} ${parseInt(days/365) > 1 ? 'years' :'year'} old` : days = `You are ${days} ${days > 1 ? 'days' :'day'} old`;
    return days
}

module.exports = {
    tellAge
}
/*


// For this exercise you need to use Date object -- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date

// - Define a function called 'tellAge' which takes an 3 arguments: a day, a month, and a year.
var tellAge = function(m,d,y){

// - Inside the function create 2 Date objects -- one for today and another for the date of birth provided in the arguments.
let today = new Date()
let dob = new Date(`${m}/${d}/${y}`)
// - Create a new variable called 'timeDiff' and as a value assign results of subtracting the date of birth from today. It will be the difference in milliseconds
let timeDiff = today - dob
// - If 'timeDiff' is less than 1 return "You can't be born in the future!"
if ( timeDiff < 1) return "You can't be born in the future!"
// - Create a new variable called 'days'
// - Convert 'timeDiff' which is in ms to days and round it with Math.floor.
// - You will have an age of a person in days.
 let days = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
// - Check if number of days bigger than 365 and then compose an output string with age in years. 
// - If it's less than 365 then compose an output string in days. 
if(days > 365){var results = `You are ${parseInt(days/365)} ${parseInt(days/365) > 1 ? 'years' :'year'} old`}else{var results = `You are ${days} ${days > 1 ? 'days' :'day'} old`}
// - Return this output string
return results
}