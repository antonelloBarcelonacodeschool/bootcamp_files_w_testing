const calc = (a, b , opt) => {
    if (!opt && b == "+" || b == "-")  opt = b, b = 0
    if (!opt && b == "*" || b == "/")  opt = b, b = 1
    return  eval(a + opt + b);
 }
 module.exports = {
     calc
 }
 // == HINT === 
 // Remember that the order is what determines which argument is which.
// == STEPS ===
// Define a function called 'calc' which takes an 3 arguments a, b, opt.
// If the third argument is missing and the second is '+' or '-' then set 'opt' to be equal to 'b' and 'b' to be equal to 0;
// If the third argument is missing and the second is '*' or '/' set 'opt' to be 'b' and 'b' to be 1.
// Then perform the calculation as in the previous exercise. (You could even call the same function from it)