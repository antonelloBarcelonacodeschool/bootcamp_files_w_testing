const filter = (arr, type, minLen) => `${arr.filter((ele )=> ele.length >= minLen && typeof ele != type)}`
module.exports = {
	filter
}
// == HINT === 
// You could have a look at Array.prototype.filter.
// == STEPS ===
// Define a function called 'filter' which takes 3 arguments: an array, a type and a minlength.
// inside it create a variable called 'newArr' (an empty array)
// use a loop and for each iteration check that the element is not of the type passed as argument 
// and that it's length is greater or equal to the min-length passed.
// if it matches these criteria push it in 'newArr'.
// otherwise skip it
// return 'newArr'.

// Pseudo code
/*
function with arguments array, datatype, minlength

	declare empty resulting array to hold items which pass the condition

	start a loop
		condition if current item is not of 'datatype' from the arguments AND it's length is bigger or equal to 'minlength' from the arguments
			if so, add item to the resulting array

	end a loop

	return resulting array

end function
*/

