const currenciesDB =  [];
const dinamicConverter = (str, arr, curr)=> {
    let results;
    if (str === "add") {
      if(currenciesDB.length < 1){
         currenciesDB.push(arr);
         results = 1;
     }else{
        let index = currenciesDB.findIndex((ele)=> ele[0] === arr[0]);
        if(index === -1) {
           currenciesDB.push(arr);
           results =  1;     
       }
       else results = NaN;
   }
}


if(str === "convert"){
  if(curr === 'usd') {
     let index = currenciesDB.findIndex((a)=> a[0] === arr[0]);
     results = arr[1] / currenciesDB[index][1];
     return !isNaN(results) ? results : 'invalid data provided!';

 }
 let exchange;
 currenciesDB.forEach(ele => ele[0] === curr ? exchange = ele[1] : null);
 currenciesDB.forEach(ele=>{
    if(arr[0] === ele [0]){
        let rate = ele[1];
        results = arr[1] *  rate / exchange;
    }
})
}
return !isNaN(results) ? results : 'invalid data provided!';
}


module.exports = {
    dinamicConverter, currenciesDB
};



// == HINT === 
// Check out Array.prototype.findIndex.
// == STEPS ===
// Define an empty array called 'currenciesDB'
// Define a function called 'dinamicConverter' which takes 3 arguments: a string, an array and a currency.
// Inside it declare a variable called 'one'.
// Split the functionality in 2 main conditions.
    // If the argument string is "add" then you should add to the 'currenciesDB' array.
    // If the argument is 'convert' then you should fetch the rate and convert.
//  ====== ADD ======
// Check first if the 'currenciesDB' is empty, if that's the case push the array to it and set result to be 1;
// If it is not empty use Array.prototype.findIndex to first check if the array is already inside 'currenciesDB'.
    // If it is not push it in and and set result to be 1;
// In case it is already present, don't push it and set result to be NaN.
//  ====== CONVERT ======
// Check first if currency argument 'curr' is equal to usd
    // If it is find it's conversion rate using Array.prototype.findIndex
    // Return the result

// If it is not usd then you need to first find the exchange rate of your currency to usd and then convert that in the desired currency.
// Return the result.


/* 
var dinamicConverter=(action, inputCur, outputCur)=>{
  if(action != 'add' && action != 'convert'){
    return "invalid data provided!"
  }
  if(action == 'add' && inputCur){
let curIdx = curDB.findIndex( (ele)=> ele[0] == inputCur[0] ) 
    if(curIdx == -1){ curDB.push(inputCur) 
    }else{
      return "invalid data provided!"
    }
  }else{
// convert or no input cur
    if(!inputCur || !outputCur ){
      return "invalid data provided!"
    }
let curIdx = curDB.findIndex( (ele)=> ele[0] == inputCur[0] ) // 0
let rate = curDB[0][1]
let convToRate = curDB.findIndex( (ele)=> ele[0] == outputCur ) // 1
if(curIdx == -1 || convToRate == -1){
  return "invalid data provided!"
}
return inputCur[1] * curDB[curIdx][1] / curDB[convToRate][1]
}
}