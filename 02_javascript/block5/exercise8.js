const strCut = (str, a, b ) => {
	const arr = []
	str.split('').forEach((ele,i)=>{
		i !== a && i !== b ? arr.push(ele) : null;
	})
	return arr.join('');
}
module.exports = { strCut };
// == HINT === 
// You can use Array.prototype.join, String.prototype.split
// == STEPS ===
// Define a function called 'strCut' which takes 3 arguments a string and two characters.
// Then define an empty array called 'arr'.
// Loop through the string and each time push each letter as long as it is not equal to any of the two characters.
// Make the array a string using join.
// Return it.