const checkAge = (m, d) =>{
	if(m > 12 || d > 31 || m < 1 || d < 1) return "Error invalid data provided"
    if (new Date(`${m}/${d}/${new Date().getFullYear()}`).toDateString() == new Date().toDateString())return "happy birhtday!"
	let diff =  new Date(`${m}/${d}/${new Date().getFullYear()}`) - new Date() 
	if(diff < 1)return "Sorry your birthday is passed for this year"
    let days = Math.floor(diff / (1000 * 60 * 60 * 24))
    if(days <= 30){ return `there are ${days} days until your birthday` };
    let months = days >= 30 ? days/30 : null
    for (let i = 0; i < Math.floor(months); i ++){ days -= 30 };
    let isPlural = (arg, strOne, strTwo) =>{
        let sentence;
        arg > 1 ? sentence = strOne : sentence = strTwo
        return sentence
    }
    months = Math.floor(months)
    days   = Math.floor(days)
    if(months < 1) return `${isPlural(days, 'there are', 'there is')} ${isPlural(days, 'days','day')} until your next birthday`
    if(days < 1  ) return `${isPlural(months,'There are','There is')} ${isPlural(months, 'months','month')} until your next birthday`
    else{
        return `${isPlural(months,'There are','There is')} ${months} ${isPlural(months, 'months','month')} and ${days} ${isPlural(days, 'days','day')} until your birthday`
    }
}
module.exports = { checkAge };
// quick solution incomplete
// var checkAge = (m, d) =>{
// 	let year = new Date().getFullYear();
// 	let ms =  new Date(year, m-1, d+1) - new Date();
// 	let days =  Math.floor(ms / (1000 * 60 * 60 * 24));
// 	let months = Math.floor(days / 30);
// 	let daysLeft = days % 30;
// 	return {months, daysLeft};
	
// }




// == HINT === 
// Use the solution form the previous exercise as a starting point here...
// == STEPS ===
// Define a function called 'checkAge' which takes 2 arguments: a month and a day.
// Before running the rest of the code check if any of the arguments is invalid (example a day greater than 31, a month greater than 12 ...)
// In case of invalid data return "Error, invalid data provided"
// In case the date of birth is today return "happy birthday!"
// Create a new variable called 'timeDiff', subtract the date of birth from today and set the result to be the value of 'timeDiff'.
// If 'timeDiff' is less than 1 return  "Sorry your birthday is passed for this year"
// Create a new variable called 'days'
// Convert 'timeDiff' which is in ms to days and round it with Math.floor.
// If days is less or equal than 30 then you don't need any further checking.
// Convert the days in months assuming that every month has 30 days.
// Subtract the days used to make up months from the days, so you end up with just the "extra" days.
// Create a function called 'isPlural' which takes three arguments, a number and two strings
    // These two strings will be two words – one singular and one plural.
    // If the number is bigger then one then return the plural string otherwise the singular.
    // We can use this function later on to output the correct message depending on how many months, days we have.
// Round both months and days using Math.floor
// Now use conditional to check if there is less than one month or less than one day, in which case you know that it will be only months or days.
// Otherwise it will be both days and months.
// Construct the sentence as required and use the helper 'isPlural' function to decide if using singular or plural forms.
