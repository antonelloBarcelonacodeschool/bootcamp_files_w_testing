const stringChop = (str, len)=>{
	if(!len) return [str];
    let arr = [], i = 0;
    for (i; i < str.length; i+= len){
        arr.push(str.slice(i, i+ len));
    }
	return arr;
}
module.exports = {
    stringChop
}
// == HINT === 
// You may check out String.prototype.substring
// == STEPS ===
// Define a function called 'stringChop' which takes 2 arguments: a string and a 'len'.
// If the second argument is missing return an array with just the string passed as argument.
// Define an empty array called 'arr' and a variable called 'i' and give it a value of 0;
// Create a 'for' loop and loop as long as 'i' is less than string.'length, increasing 'i' by 'len' each time.
// Inside the loop call String.prototype.substring and call it passing 'i' and i + 'len', and push its return to 'arr'.
// The outside the loop return 'arr'.