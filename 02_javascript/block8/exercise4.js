const tally   = (arr, action) =>{
    const obj = {}
    arr.forEach( ele => obj[ele] = obj[ele]+1 || 1 )
    if (action === 'obj' || !action) {
        return obj
    }else{
        const a = []
        for (let key in obj){
            a.push([key, obj[key]])
        }
        return a;
    }
}
module.exports = {
    tally
}
// == HINT === 
// You can recycle the previous exercise for most part.
// == STEPS ===
// Define a function called 'tally' which takes 2 arguments: an array and an action.
// Inside it define an empty object called 'obj'.
// Loop through the array (argument) and like in the previous exercise add each property to 'obj' if not there or increase its value by one if it is.
// If the argument action is 'obj': return the object and you are done!
// Otherwise create an empty array
    // Loop through the object which now contains the info you need and push them to the array you just created inside another array.
    // Return the array.