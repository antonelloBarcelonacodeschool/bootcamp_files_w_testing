const sorter = (arr, order) => {
	if(order){
		if(order !== 'ascending' && order !== 'descending'){
			return  (`wrong order provided ${order} please provide us with ascending or descending order instructions`)
		}	
	}
	const orderAscending = ()=>{
		if(arr[i] > arr[j]){
			let temp = arr[i]
			arr[i]   = arr[j], arr[j]   = temp
		}
	},
	orderDiscending = () => {
		if(arr[i] < arr[j]){
			let temp = arr[j]
			arr[j]   = arr[i], arr[i]   = temp
		}
	}
	let i = 0, j;
	for (i; i < arr.length; i++){
		for(j = i+1 ;j <arr.length; j++ ){
			order == 'ascending' || !order ? orderAscending() :orderDiscending()
    	}
	}
	return arr
}

module.exports ={
    sorter
}
// == HINT === 
// You can use a nested loop to sort your array.
// == STEPS ===
// Define a function called 'sorter' which takes 2 arguments, an array and a string (the 'order')
// First check that the order has being passed, and if so make sure that is either ascending or descending.
// If isn't either of these 2 then return the error message.
// Define two functions, one called 'ascending' and one called 'descending'.
// In the 'ascending' function check if arr[i] is greater than arr[j] (assuming you are using these 2 letter for your loops!)
	// then swap them.
// In the 'descending' function check if arr[i] is less than arr[j] (assuming you are using these 2 letter for your loops!)
	// then swap them.
//Then in the inner (nested) loop check if the order is ascending or missing: call the ascending function, if is descending call the descending function.
// Then outside the loop return the arr.