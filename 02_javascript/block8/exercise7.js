let database = []

let movie_model = {
      Actors: null,
      Director: null,
      Awards: null,
      Genre: null,
      Poster: null,
      Released: null,
      Title: null,
      Plot: null
}

let create_movie = (movie) => {
   const temp = movie_model
   for(let key in movie_model){
     temp[key] = movie[key]
   }
   return temp
}

let get_movie = async() => {
  const title = prompt('Input a movie title')
  const r = await fetch(`https://omdbapi.com?t=${title}&apikey=thewdb`)
  const result = await r.json()
  const genres = result.Genre.replace(/\s/g, '').split(',')
  for(let ele of genres){
     let index = database.findIndex( obj => obj.Genre === ele ), index2;
     index === -1
     ? database.push({Genre:ele,movies:[create_movie(result)]})
     : (
       index2 = database[index].movies.findIndex( el => el.Title === result.Title),
       index2 === -1 
       ? database[index].movies.push(create_movie(result))
       : null
     )
  }
  return database;
}
get_movie().then( data => console.log(data) )