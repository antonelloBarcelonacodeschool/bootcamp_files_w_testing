var recursive = (arr) => {
var count = 0
var a = []
var pusher = (arr, count) => {

	if(count < arr.length) {
		a.push([arr[count].name, arr[count].age])
		return pusher(arr, count+=1)
	} else {
		return a
	}
}
return pusher(arr,count)
}


// old version

// const recursive = (arr, num, a) => {
// 	if(num < arr.length) {
// 		a.push([arr[num].name, arr[num].age])
// 		num++;
// 	} else {
// 		return
// 	}
// 	return (recursive(arr, num, a));
// }
module.exports = {
    recursive
}
// == HINT === 
// You can use recursion as a loop...
// == STEPS ===
// Declare a function called 'recursive' which takes 3 arguments: an array of objects ('arr'), a number ('num') and an empty array('a').
// If the 'num' is less than the length of 'arr' 
	// push to the array ('a') an array which contains the name and age: example : a.push([name, age])
	// increase the value of 'num' by one.
// If the 'num' is NOT less than the length of 'arr' just return.
// then call the function recursively passing all arguments.