let incByStep =(num, step, maxN, results = [])=>{
return num < maxN ? (
results.push(num),
incByStep(num+step, step, maxN, results) ) : results
}

module.exports = {
    incByStep
}