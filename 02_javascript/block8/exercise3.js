const tally   = arr =>{
    const obj = {}
    arr.forEach( ele => obj[ele] = obj[ele]+1 || 1 )
    return obj
}

let tally =(a)=> a.reduce( (obj,item)=> ({...obj, [item]: (obj[item]+=1) || 1 }), {} )

module.exports = {
    tally
}
// == HINT === 
// The || operator can be handy here...
// == STEPS ===
// Create a function called 'tally' which takes an array as argument.
// Inside it define an empty object called 'obj'.
// Loop through the array and each iteration check if our object ('obj') has a key corresponding to the array element:
    // If it does then increase it's value by one, otherwise add it in with a value of 1.
// Then outside the loop return the object.