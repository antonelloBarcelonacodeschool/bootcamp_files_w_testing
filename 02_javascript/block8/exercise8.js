var key = '16909a97489bed275d13dbdea4e01f59'

var getCurrentWeather = () => {
      return navigator.geolocation.getCurrentPosition( async pos => {
         const { latitude:lat, longitude:lon } = pos.coords
         const r = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${key}&units=metric`)
         const data = await r.json() 
         console.log({
                     name: data.name,
                     description: data.weather[0].description,
                     temp: data.main.temp,
                     temp_min: data.main.temp_min,
                     temp_max: data.main.temp_max,
                     wind: data.wind.speed,
                     humidity: data.main.humidity
                 })
      })  
}

getCurrentWeather()