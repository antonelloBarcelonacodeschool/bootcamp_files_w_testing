const Server = function(arg){
    const self = this;
    arg = arg || -1
    this.app = {
        get: function (params){
            var action  = params.split('/')[1], 
            name    = params.split('/')[2],
            amount  = params.split('/')[3];
            if( action == 'sort'){
                var order = amount;
            }
            this.actions = {
                find:(name)=> {   
                    if(name)return self.accounts.filter( a => a.id == name)[0]
                    return self.accounts
                },
                sort: (by)=>{
                    if(!by || by == 'name') {
                        if(order == 'asc' ) return self.accounts.sort((a,b) => a.name.localeCompare(b.name))
                        if(order == 'desc') return self.accounts.sort((a,b) => b.name.localeCompare(a.name))
                    }else{
                        if(order == 'asc' ) return self.accounts.sort((a,b) => a.amount - b.amount )
                        if(order == 'desc') return self.accounts.sort((a,b) => b.amount - a.amount )
                    }
                }
                
            }
            if(this.actions[action]){
                return this.actions[action](name)
            }
            else return '404 page not found'
            
        },
        post:function(params){
            const noIndex = "Account not found"
            let action  = params.split('/')[1].trim(), 
                name    = params.split('/')[2].trim(),
                amount  = Number(params.split('/')[3]),
                newName = params.split('/')[4];

            let criteria;
            !isNaN(name) && action == 'update' || !isNaN(name) && action == 'delete' ? criteria = 'id' : criteria = 'name'
            let index      = self.accounts.findIndex(a => a[criteria] == name);
            this.actions = {
                new:    () => {
                    arg++
                    if(index === -1){
                        self.accounts.push({name, amount, id:arg})
                    }
                    else{
                        return `Account ${name} already present in db`
                    }
                },
                delete: () => {
                  if  (self.accounts[index]) {
                      self.accounts.splice(index, 1)
                  } else{
                      return noIndex;
                  }
                },
                update: () => {
                    if(self.accounts[index]){  
                        self.accounts[index]['name']   = newName
                        self.accounts[index]['amount'] = amount
                    }else{
                        noIndex;
                    }
                },
                withdraw: (index, val) =>{
                    if(self.accounts[index]) return self.accounts[index].amount -= val 
                    else return noIndex;
                },
                deposit:  (index, val) =>{
                    if(self.accounts[index]) return self.accounts[index].amount += val 
                    else return  noIndex;
                }
            }
            return this.actions[action](name, amount);
        }
    };
    this.accounts =[]
}


module.exports = {
    Server
}

// == HINT === 

// This function is the simulation of an actual web server, taking the URLs as the argument and pretending to perform actions, which a normal server would do.
// And every URL passed as an argument triggers one of the methods inside your 'app' object.

// == STEPS ===
// Declare a constructor function called Server which takes an argument (a number), which will be used as an id, if this is not provided it should default to -1.
// Declare a variable called self and give it this as its value.
// Create an object called app using the keyword "this".
//==============================GET========================================//
// In app add a key of "get" with a value of a function, this function takes on argument.
    // Split that argument by the "/"
    // assign action, name and the amount each to a variable.
    // If action is "sort" then declare also order and amount as empty variables.
    // Create an object called actions using the keyword "this".
    // Inside the actions define a function called find, adding "find" as the key and the function as the value.
    // This function will take one argument a name / id, and using Array.prototype.filter will give you back the account.
    // Still using key value pairs format define a function called sort which takes one argument:
        // "by" and depending on its value as well as the order it will sort the accounts appropriately.
    // Now that our methods are defined as objects we can use the action variable that we have previously defined to call the appropriate method.
//==============================POST========================================//
// In app add a key of "post" with a value of a function, this function takes on argument.
    // Split that argument by the "/"
    // assign action, name, amount and newName each to a variable.
    // Define an empty variable called criteria, its value will depend on the action:
        // if the action is delete or update it will be id, otherwise name.
    // Use Array.prototype.findIndex to get the index of the account using the id and assign it to a variable called index.
    // Create an object called actions using the keyword "this".
    // Within this.actions we need to create 5 methods: new, delete, update, withdraw deposit, each of them using the key, value pair format.
        // New method will first increase the amount 
        // Using the index that we should already have: check, if it is -1 add it in, otherwise inform the user that it already exists.
    // Delete use the index to splice the account out of the array (or any other Array method of your choice)
        // if the account is not present inform the user.
    // Update like delete use the index to select the appropriate account and perform the update.
        // If the account is not there inform the user.
    // Withdraw
        // Withdraw takes 2 arguments the index and the amount, it will use the index as update and delete and it will subtract the amount from the total on that account.
        // If the account is not there inform the user.
    // Deposit
        // Withdraw takes 2 arguments the index and the amount, it will use the index as update and delete and it will add the amount to the total on that account.
        // If the account is not there inform the user.
    // Now that our methods are defined as objects we can use the action variable that we have previously defined to call the appropriate method.

    

