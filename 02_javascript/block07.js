// exercise1
// write a function called moviesDB which we use to create a movie database.
// this function should take 3 arguments an array(the actual db), a genre, and an object (the movie)
// it should check if the genre exists and if it doesn't it should add it on.
// it should also check if the movie exists and add it on if it doesn't.
// see the example of the data structure you MUST follow.
// if the movie is already present it should not add it again and it should instead return the following message:
// 'the movie the < YOUR_MOVIE_GOES_HERE > is already in the database!'

DB = [
    {
        genre:'thriller', 
        movies:[
            {
                title:'the usual suspects', release_date:1999
            },             
            {
                title:'seven', release_date:2000
            }
        ]},
        {
        genre:'comedy', 
        movies:[
            {
                title:'pinapple express', release_date:2008
            },
            {
                title:'Y tu mama tambien', release_date:2000
            }
        ]}
]

EXERCISE 2
//To practice passing data between functions let's create a crypto converter…
//You need to create several functions, each will be responabile for its own taks and to call the next function.
// addCurrency
// findcurrency
// converter
// tellConversion
//You should exclusively call addCurrency and it will call the others
// add currency takes three arguments 
// a coin, its value and an array of coins (our coins database)
// the coin should be an object with the following structure:

// {
//      coin:'coin_here', 
//      rate:CONVERSION_RATE_TO_USD_TYPE_NUMBER
// }

// addCurrency should first check if the coin already exists in the DB and if it doesnn't it should add it on and return the following:
// "New coin {YOUR_NEW_ADDED_COIN_GOES_HERE} added to Database"
// If the coin does exist in the DB it should call findcurrency which should retreive the conversion rate of the given currency and then passing that should call convert 
// which s in charge of doing the actual conversion.
// However it is TellConversion who is in charge to return the final message to the user.
// "You will receive {AMOUNT} usd for your 2 {COINS_NAME}"
// Please make sure that when adding the new currency the message capitalize the coin name.
// Please make sure that in the final message
***Your solution goes after this line***




EXERCISE 3

    // Create a constructor function called bankAccount, which has 3 methods:
    // withdraw which takes away from the accounc balance, 
    // deposit which adds to it,
    // and balance which returns the balance.
    // the function should work with or without an initial amount.
    Example
    var account = new bankAccount(10)
    account.withdraw(2)
    account.withdraw(5)
    account.deposit(4)
    account.deposit(1)
    account.balance() // 8 

***Your solution goes after this line***






EXERCISE 4


    Implement a representation of the universe where matter and energy is conserved. To do so implement one object called Universe that contains two objects within: Matter and Energy. If matter is destroyed; that is say we call Universe.Matter.destroy(5), then the amount of energy in the universe needs to be increased so that if we call Universe.Energy.total() we should obtain a total value of energy that has increased +5 compared to the energy value previous to calling Universe.Matter.destroy(5). Of course the total amount of Matter obtained by calling Universe.Matter.total()has been reduced by 5 compared to the initial value.
        - Implement this objects using context
        - The Matter and Energy objects are defined within an object called Universe
        - No other variable should be defined out of the Universe object
        - Also implement the create methods for both matter an energy which are opposite to their counterparts
        - You should be able to give an initial amount to either the energy or the matter, otherwise shiould defaut to 0.
    Example

    var universe = new Universe(10, 'matter')
    Universe.Matter.total() // 10 
    Universe.Energy.total() // 0 

    // or with no initial amount 
    var universe = new Universe()
    Universe.Matter.total() // 0 
    Universe.Energy.total() // 0 
    Universe.Matter.destroy(5) // 0 
    Universe.Matter.total() // -5 
    Universe.Energy.total() // 5 
    Universe.Energy.destroy(-5) // 0 
    Universe.Matter.total() // -10 
    Universe.Energy.total() // 10 
        > Notes: Initially make your universe contain 0 matter and energy. Destroying a negative amount of energy of matter is equal to creating a positive amount of each and viceversa for creating matter or energy.

***Your solution goes after this line***








***Your solution goes after this line***





EXERCISE 5

Without Googling how to shuffle elements inside an array in javascript  create a function called shuffle and inplement in it your own algorithm to shuffle the elements inside of the given array.

Example: 
var arr = ['one','two','three','four']
shuffle(arr)
(4) ["three", "one", "four", "two"]
shuffle(arr)
(4) ["two", "one", "three", "four"]
shuffle(arr)
(4) ["one", "two", "three", "four"]
shuffle(arr)
(4) ["three", "two", "four", "one"]


***Your solution goes after this line***




EXERCISE 6
/* Family affairs*/


/*
Jenny from perfectFamily is unhappy and is looking for a new family, help her! 

First, she is looking for a family without a mother so she would fit there easily, if such family is not available, then her second choice would be to find a family without any kids. If she has some bad luck with that, then her choice would be a family with the oldest kid(s) possible. 

Function name is familyAffairs, first argument is a perfect family, second argument is an array with other families.

Test case 1 with a family without mother:

let perfectFamily = {
    father:{ name:'Mike', age:44, height:179 },
    mother:{ name:'Jenny', age:40, height:168 },
    son:{ name:'Pablo', age:16, height:165 }
}

let otherFamilies = [
{Smiths:{
    father:{ name:'Jake', age:38, height:182 },
    mother:{ name:'Viola', age:36, height:172 },
    son:{ name:'Donny', age:14, height:180 }
    }
},
{Morenos:{
    father:{ name:'Juan', age:42, height:188 },
    daughter:{ name:'Julia', age:10, height:149 }
        }
},
{Tanakas:{
    father:{ name:'Kioto', age:39, height:172 },
    mother:{ name:'Junko', age:42, height:164 },
    son:{ name:'Bundo', age:24, height:164 }
        }
}
]

Expected output: Yay! Jenny moved to Morenos


Test case 2 with all families with mothers but one without kids:

let perfectFamily = {
    father:{ name:'Mike', age:44, height:179 },
    mother:{ name:'Jenny', age:40, height:168 },
    son:{ name:'Pablo', age:16, height:165 }
}

let otherFamilies = [
{Smiths:{
    father:{ name:'Jake', age:38, height:182 },
    mother:{ name:'Viola', age:36, height:172 }
    }
},
{Morenos:{
    father:{ name:'Juan', age:42, height:188 },
    daughter:{ name:'Julia', age:10, height:149 },
    mother:{ name:'Kate', age:36, height:172 }
        }
},
{Tanakas:{
    father:{ name:'Kioto', age:39, height:172 },
    mother:{ name:'Junko', age:42, height:164 },
    son:{ name:'Bundo', age:24, height:164 }
        }
}
]

Expected output: Yay! Jenny moved to Smiths



Test case 3 with all families with mothers and kids:


let perfectFamily = {
    father:{ name:'Mike', age:44, height:179 },
    mother:{ name:'Jenny', age:40, height:168 },
    son:{ name:'Pablo', age:16, height:165 }
}

let otherFamilies = [
{Smiths:{
    father:{ name:'Jake', age:38, height:182 },
    mother:{ name:'Viola', age:36, height:172 },
    son:{ name:'Donny', age:14, height:180 }
    }
},
{Morenos:{
    father:{ name:'Juan', age:42, height:188 },
    daughter:{ name:'Julia', age:10, height:149 },
    mother:{ name:'Kate', age:36, height:172 }
        }
},
{Tanakas:{
    father:{ name:'Kioto', age:39, height:172 },
    mother:{ name:'Junko', age:42, height:164 },
    son:{ name:'Bundo', age:24, height:164 }
        }
}
]


Expected output: Yay! Jenny moved to Tanakas

*/


EXERCISE 7 

/* Humans invasion */
// No tests for this one since it will be using a random choice for the battle 🤪

/*
As we run out or resources on Earth we are planning an invasion on planet Otatop which seems to have very suitable atmosphere for us. But there is a tiny problem -- it is already inhibitit by some local creatures. And they are willing to protect it from us (how dare they!).
So we assemled an army of stormtroopers willing to defeat and conquer. 
First battle is happening on Otatop, if we defeat them there -- war is over!!!
If not, next battle is half-way, on the Moon! From there, if those annoying aliens will defeat us, the battle is on Earth. Loosing on Earth means end of game for us. If we won on Earth, moving back to the Moon and so on. 
So the race loosing the battle on their home planet lose the game and seize to exist. 
Alternatively, the army first ti perish completely also means kaput for the player and end game. If unit is perished completely, it can't go to the next battle, duh.

Example data:

let humans = [
{unit:'karateMasters', qty: 3000000, strength: 5, rateOfFire: 20, stamina: 10},
{unit:'teslaTanks', qty: 500, strength: 100, rateOfFire: 5, stamina: 100}
{unit:'bostonDynamicsSpot', qty: 200, strength: 50, rateOfFire: 15, stamina: 60}
]

let aliens = [
{unit:'motherLord', qty: 1, strength: 500, rateOfFire: 50, stamina: 300},
{unit:'yellowCreature', qty: 250, strength: 200, rateOfFire: 6, stamina: 90}
{unit:'slimyThing', qty: 2500000, strength: 3, rateOfFire: 30, stamina: 7}
]

let locations = ['Earth','Moon','Otatop']

*/





