const howManyCaps = str =>{
    const capitals = [];
    const isCapital = ele => ele === ele.toUpperCase() && ele.toUpperCase() !== ele.toLowerCase()
    str.split('').forEach(ele =>{
         isCapital(ele) ? capitals.push(ele) : null
    });
    return `There are ${capitals.length} capitals and these are ${capitals}`
}

module.exports = {
    howManyCaps
}

// == HINT === 
// White spaces are considered uppercase in javascript!
// == STEPS ===
// Define a function called 'howManyCaps' which takes a string as argument.
// Inside it define an empty array called 'capitals'.
// Create a function called 'isCapital' which takes an argument, and checks if it is uppercase, returning true of it is and false otherwise.
// Loop inside the string (argument) and for each iteration call your 'isCapital' function on each element.
// If it returns true push the element to 'capitals' array.
// Make up the sentence as required using the 'capitals' array.

