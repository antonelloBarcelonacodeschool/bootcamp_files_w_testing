const uniqueElements = (arr, max) =>{
    const newArr = []
    arr.forEach(ele=>{
        !newArr.includes(ele) && typeof ele == 'number' && ele > max ? newArr.push(ele) : null
    })
    return `old array ${arr} \n new array ${newArr}`
}
module.exports = {
    uniqueElements
}

// == HINT === 
// Like the previous exercise plus 2 extra conditions ...
// == STEPS ===
// Define a function called 'uniqueElements' which takes an array and a number ('max') as arguments.
// Inside it create an array called 'newArr'
// Loop inside the array (argument) and for each iteration check if the element is not present in the 'newArr', 
    // if it is a number,
    // and that the length of the 'newArr' is less than 'max', if all is ok push it in.
// Make up the sentence as required using the 'newArr' array.
// Return the sentence.