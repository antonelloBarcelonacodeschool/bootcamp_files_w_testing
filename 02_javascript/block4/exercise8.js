const uniqueElements = arr =>{
    const newArr = []
    arr.forEach(ele=>{
        !newArr.includes(ele) ? newArr.push(ele) : null
    })
    return `old array ${arr} \n new array ${newArr}`
}
module.exports = {
    uniqueElements
}

// == HINT === 
// Array.prototype.includes or Array.prototype.indexOf could be of help.
// == STEPS ===
// Define a function called 'uniqueElements' which takes an array as argument.
// Inside it create a new Array called 'newArr'
// Loop inside the array (argument) and for each iteration check if the element is present in the 'newArr' and push it if is not there.
// Make up the sentence as required using the 'newArr' array.
// Return the sentence.