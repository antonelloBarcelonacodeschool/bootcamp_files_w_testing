const isTrue = val => val ? true : false;
module.exports = {
    isTrue
}

// == HINT === 
// You can get away with a single condition!
// == STEPS ===
// Define a function called isTrue which takes one argument.
// Inside it check if the argument passed is not falsy, 
// Return true if it is not and false if it is.


