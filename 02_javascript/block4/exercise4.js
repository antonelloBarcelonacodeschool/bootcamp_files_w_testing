const checker = str => {
    const commas = [], questionmarks = []
    str.split('').forEach(ele => {
        if(ele == "?") questionmarks.push(ele)
        if(ele == ",") commas.push(ele)
    })
    return `${commas.length} ${commas.length > 1 ? "commas" : "comma"} ${questionmarks.length} ${questionmarks.length > 1 ?  "question marks" : "question mark"}`
}
module.exports = { checker };

// == HINT === 
// Create two separate arrays, one for commas and one for question marks.
// == STEPS ===
// define a function called checker which takes a string argument.
// define two separate arrays, one for commas and one for question marks.
// loop through the string:
// when you find a comma push it to the commas array
// when you find a question mark push it to the question marks array.
// use these arrays to make up the sentence as required.
// keep in mind that if there is one comma or question mark it should say comma / question mark if there are more it should say commas / question marks


