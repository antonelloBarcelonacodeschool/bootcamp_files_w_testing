const check_who_is_older = (name1,age1,name2,age2) =>{
	const sentence = (n, a, n2, a2) => `${n} age ${a} is older than ${n2} age ${a2}`;
	if (age1 > age2) return sentence(name1, age1, name2, age2);
	if (age2 > age1) return sentence(name2, age2, name1, age1);
	return "they are of the same age";
	
}




module.exports = {
    check_who_is_older
}

// == HINT === 
// You could create an inner function which return the sentence and pass the names as arguments ...
// == STEPS ===
// Define a function called check_who_is_older which takes 4 arguments: 2 names and 2 ages.
// Compare the ages and see which is greater.
// If the ages are the same return "they are of the same age";
// Otherwise return a sentence like the one below replacing the names and ages with the name/age of the older person.
// ${name} age ${age} is older than ${name2} age ${age2}`
