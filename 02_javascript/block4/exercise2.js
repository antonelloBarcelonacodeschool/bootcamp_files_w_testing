// !!! excludes zeros
// var is_an_even_number = arr =>{
// 	let count = 0;
// 	arr.forEach(el =>{
// 		if (typeof el === 'string' || typeof el === 'number' || typeof Number(el) == 'number'){
// 			if(el !=0 && el % 2 === 0){
// 				count ++  
// 			}}
// 		});
// 	return count;
// }

// includes zeros
var is_an_even_number = arr =>{
	let count = 0;
	arr.forEach(el =>{
		if (typeof el === 'string' && el.length > 0 || typeof el === 'number' || el.length == 1){
// OR if (/\d/.test(el)){
			if(el % 2 === 0){
				count ++  
			}}
		});
	return count;
}

module.exports ={
	is_an_even_number
}

// == HINT === 
// You may need isNaN and the modulus operator.
// == STEPS ===
// Define a function called is_an_even_number which takes an array as argument.
// Inside it define a variable called count and give it a value of 0.
// Loop through the array and for each iteration check if an item is a number and if it's divisible by 2, and if it is – increase count by one.
// Return count  

