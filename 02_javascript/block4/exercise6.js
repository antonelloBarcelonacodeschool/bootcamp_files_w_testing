// const numberConverter = arr =>{
//     let numbers = [], unconvertable = [];
//     arr.forEach( ele =>{
//         if(typeof ele !== 'number' || ele.length > 1 || ele == '' || ele.length == 0){ !isNaN(ele) ? numbers.push(ele) : unconvertable.push(ele) };
//     })
//     if(numbers.length > 1) return `${numbers.length} were converted to numbers  ${numbers} ${unconvertable.length} couldn't be converted`
//     return 'no need for conversion'
// }

let numberConverter = arr => {
	var converted = [], unconvertable = []
	arr.forEach((ele) => {
		if(typeof ele != 'number'){
			!isNaN(Number(ele)) && ele.length != 0 ?
			converted.push(ele) : unconvertable.push(ele)
		}
	})

	return converted.length == 0 && unconvertable.length == 0? `no need for conversion` : `${converted.length} were converted to numbers: ${converted}, ${unconvertable.length} couldn't be converted`
}





// function convert(arg, obj) {
// 	// arg is a number, don't convert
// 	if (typeof(arg) == 'number') {
// 		obj.numbers.push(arg);
// 		return obj;
// 	}
// 	// arg is a string
// 	if (typeof(arg) == 'string') {
// 		// arg is a number and not empty string
// 		if (!isNaN(arg) && arg != '') {
// 			obj.converted++
// 			obj.convertedList.push(Number(arg));
// 			return obj;
// 		// arg is not a numer
// 		} else {
// 			console.log(arg);
// 			obj.unconverted++;
// 			obj.unconvertedList.push(arg);
// 			return obj;
// 		}
// 	}
// 	// arg is an array
// 	if (Array.isArray(arg)) {
// 		if (arg.length == 0) {
// 			obj.unconverted++;
// 			obj.unconvertedList.push(arg);
// 			return obj;
// 		} else {
// 			for (const item of arg) {
// 				// recurse on array item
// 				obj = convert(item, obj);
// 			}
// 			return obj;
// 		}
// 	} else {
// 		obj.unconverted++;
// 		obj.unconvertedList.push(arg);
// 		return obj;
// 	}
// }

// function numberConverter(arg) {

// 	let obj = {
// 		converted: 0,
// 		unconverted: 0,
// 		convertedList: [],
// 		unconvertedList: [],
// 		numbers: []
// 	};
	
// 	obj = convert(arg, obj);

// 	console.log(arg, obj);

// 	if (obj.numbers.length == arg.length) {
// 		return "no need for conversion";
// 	} else {
// 		return  obj.convertedList.length + ' were converted to numbers: ' + obj.convertedList.join(',') + ', ' + obj.unconverted + " couldn't be converted";
// 	}
// }


module.exports ={ numberConverter };

// == HINT === 
// You should use isNaN and typeof.
// == STEPS ===
// Define a function called 'numberConverter' which takes an array as argument.
// Inside it define two arrays, 'numbers' and 'unconvertable'.
// Loop through the array (argument) and for each element first check if it is not already a number
// Then check if it is not NaN, in which case push it to the 'numbers' array, otherwise push it to the 'unconvertable' array.
// Make up the sentence as required using the arrays.
// Return the sentence.