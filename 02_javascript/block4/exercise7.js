const boolanChecker = (arr, max = Infinity) =>{
	let bools= []
	arr.forEach(ele =>{
		typeof ele === 'boolean' && bools.length < max ? bools.push(ele) : null
	})
	return `${bools.length} booleans were found ${bools}`
}



// #2
// let booleanChecker = (array, maxCapacity = Infinity) => {
// 	let counter = 0
// 	let bool = array.filter((item) => {
// 		if(counter < maxCapacity && typeof item === "boolean"){
// 			counter++
// 			return `${item}` 
// 		}
// 	}, counter);
// 	return `${bool.length} booleans were found ${bool}`
// }


// #3
// let booleanChecker = (array, maxCapacity) => {
//     let bool = [];
//     bool = array.filter((item) => typeof item === "boolean");
//     if (!maxCapacity) {
//         boolCounter = bool.length;
//     } else if (bool.length > maxCapacity) {
//         bool.length = maxCapacity;
//     } 
//     return `${bool.length} booleans were found ${bool}`
// }

module.exports = {
	boolanChecker
}

// == HINT === 
// You may give 'max' a default value to Infinity in case is not passed ... 
// == STEPS ===
// Define a function called 'boolanChecker' which takes 2 arguments, an array and a number ('max').
// Inside it define an array called 'bools'.
// Loop through the array (argument) and for each iteration check if the typeof each element is a boolean.
// If it is and the length of the 'bools' array is still not less than 'max', push the element.
// Make up the sentence as required using the 'bools' array.
// Return the sentence.