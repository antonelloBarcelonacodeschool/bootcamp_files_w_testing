const check_types = a => {
    let types = []
    a.forEach( el => {
        !types.includes(typeof el) ? types.push(typeof el) :null
    })
    return types.length
} 

module.exports ={
    check_types
}

// == HINT === 
// You should use typeof
// == STEPS ===
// Define a function called check_types which takes an array as argument.
// Inside it define an empty array called 'types'.
// Loop inside the array (argument) and check if the 'types' includes the typeof each element, and if it doesn't, push it in.
// Then outside the loop return the length of 'types'.