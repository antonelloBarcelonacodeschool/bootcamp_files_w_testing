//const arr = ['milk','cheese','car','lime']
// const takeAll = arr => String(new Array(arr.reverse()));
//module.exports ={ arr, takeAll };
const takeAll = arr => {
    let reversed = arr.reverse()
    return reversed
}
module.exports ={ takeAll };
// == HINT === 
// You can use Array.prototype.reverse.
// == STEPS ===
// Defined an array with the content as shown in the example.
// Define a function called takeAll which takes our variable as its solo argument.
// Reverse the array using Array.prototype.reverse,
// Make it a string using String
// Return it.

