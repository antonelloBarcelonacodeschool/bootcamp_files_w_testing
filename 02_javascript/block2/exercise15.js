let longestString =(arr,minL)=>{
	let longestEle = ''
	arr.forEach( (ele)=>{
		if(ele.length > minL){
			longestEle = ele
		}
	})
	return longestEle
}

module.exports = {
	longestString
}