// const arr = ['cheese','salame','bread','water','pizza'], position = 2;
// const splicer = (arr, position)=>{
//     arr.splice(position,1)
//     return String(arr)
// }
// module.exports = { arr, position, splicer };
const splicer = (arr, position)=> {
	arr.splice(position,1)
	return arr
	}

module.exports = { splicer };
// == HINT === 
// You can use Array.prototype.splice.

// == STEPS ===
// Declare a an array called arr and give it the values as shown in the example.
// Declare a variable called position and give it a value of 2.
// Create a function called splicer which takes our 2 variables as arguments.
// It uses Array.prototype.splice to remove the one at the given index.
// It then returns the array.