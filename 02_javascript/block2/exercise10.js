const arr = ['glass','car','watch','sofa','macbook']
const findPosition = (arr, ele)=> arr.indexOf(ele)

module.exports = {
    arr, findPosition
}
// == HINT === 
// You could use Array.prototype.indexOf
// == STEPS ===
// Create an array called arr as shown in the example.
// Define a function called findPosition which takes 2 arguments, an array and a string or number.
// using Array.prototype.indexOf return the index of the second argument in the array (arr)
