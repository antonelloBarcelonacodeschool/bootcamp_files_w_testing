// const arr =[1,2,3,4,5,6,7,8,9,0,3,4,523,44,3454]
// const removeAll = arr => {
//     arr.splice(0)
//     return String(arr)
// }
// module.exports = {
//     arr, removeAll
// }
const removeAll = arr => {
     arr.splice(0)
     return arr
}
module.exports = { removeAll }
// == HINT === 
// Array.prototype.splice can work with just one argument...
// == STEPS ===
// Declare a variable called arr and give it the values as shown in the example.
// Declare a function called removeAll which takes the array as argument.
// Remove all it's content by giving only the index of very first element.
// Make it a string
// Return the array