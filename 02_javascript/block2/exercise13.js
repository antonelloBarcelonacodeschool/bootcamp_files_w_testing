const str = 'I,Really,Like,Pizza'
const characterRemover = (str, charct)=> str.split(charct).join(' ');
module.exports = {
    str, characterRemover
}
// == HINT === 
// You could use String.prototype.split and String.prototype.join.
// == STEPS ===
// Declare a variable called str which contains the string shown in the example.
// use String.prototype.split to remove the unwanted character.
// Use String.prototype.join to make it a string again.