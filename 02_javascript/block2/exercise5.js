// const arr = ['car','soap','banana','tv','toothbrush'];
// const removeFirstAndLast = arr => {
//     arr.splice(0,1)
//     arr.splice(-1)
//     return String(arr)
// }
//module.exports = { arr,removeFirstAndLast };
const removeFirstAndLast = arr => {
    arr.splice(0,1)
    arr.splice(-1)
    return arr
}
module.exports = { removeFirstAndLast };
// == HINT === 
// You can use Array.prototype.splice
// == STEPS ===
// Declare an array with the values as shown in the example.
// Declare a function called removeFirstAndLast which takes our array as argument.
// Remove one element starting from index 0.
// Remove one element with index -1.
// Make the array a string
// Return it.