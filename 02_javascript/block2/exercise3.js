// const arr =  ['banana','apple','orange'],
//     arr2  =  ['tv','dvd-player','playstation'],
//     pos   =  2;
// const swap = (arr, arr2, pos ) =>{
//     let temp = arr[pos]
//     arr[pos]=arr2[pos], arr2[pos] = temp
//     return String([arr, arr2])
// }

// module.exports ={
//     arr, arr2, pos, swap
// }
const swap = (arr, arr2, pos ) =>{
    let temp = arr[pos]
    arr[pos]=arr2[pos], arr2[pos] = temp
    return [arr, arr2]
    // OR return [arr[pos],arr2[pos]]=[arr2[pos],arr[pos]]
}


module.exports ={ swap }
// == HINT === 
// Consider making a temporary variable to help you with the swap.
// == STEPS ===
// Define a variable called arr and assign it the value as shown in the example.
// Define a variable called arr2 and assign it the value as shown in the example.
// Define a variable called pos and give it a value of 2.
// Define a function called swap.
// Inside it, create a variable called temp, and assign to it the value of arr at index pos.
// Assign the value of arr2 at index pos to arr at index pos.
// Assign the value of temp to arr2 at index pos.
// Place the two arrays in another array
// Stringify them and return them.