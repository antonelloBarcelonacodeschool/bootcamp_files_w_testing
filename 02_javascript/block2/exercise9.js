// const arr = ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office'];
// const concatenator = arr => String([].concat(arr));

// module.exports = {
//     concatenator,
//     arr
// }
const concatenator = arr => [].concat(arr);

module.exports = {
    concatenator
}

// == HINT === 
// Use Array.prototype.concat
// == STEPS ===
// Create a variable called arr with the values as shown in the example.
// Define a function called concatenator.
// Inside define an empty array.
// Use Array.prototype.concat to join this empty array with arr.
// Make it a string
// Return it