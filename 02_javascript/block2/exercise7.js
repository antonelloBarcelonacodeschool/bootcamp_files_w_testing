// const arr = ["one","two","three","four"];
// const pusher = arr =>{
//     const arr2 =  [];
//     arr2.push(...arr)
//     return String(arr2)
// }
// module.exports = { arr, pusher };
const pusher = arr => [...arr]
module.exports = { pusher };
// == HINT === 
// Don't modify the original array.
// == STEPS ===
// Create an array called arr and assign it the value as shown in the example.
// Create a function called pusher which tales one argument, an array
// Inside the function declare a new array called arr2
// Push all the elements from arr into arr2, make sure to avoid nested arrays, you need to push every item from arr to arr2, not the entire arr as a single element.
// Return arr2 as a string