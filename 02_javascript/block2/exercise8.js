// const arr = ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office'];
// const take_and_push = (arr, indOne, indTwo)=>{
// 	const newArr = []
// 	newArr.push(arr[indOne], arr[indTwo])
// 	return String(newArr);
// }
// module.exports = {
//     arr, take_and_push
// }
const take_and_push = (arr, indOne, indTwo)=>{
	const newArr = []
	newArr.push(arr[indOne], arr[indTwo])
	return newArr;
}
module.exports = {
    take_and_push
}
// == HINT === 
// Use square bracket notation to get the elements form the array.
// == STEPS ===
// Create an array called arr as shown in the example.
// Create a function called take_and_push which takes 3 arguments, an array and 2 indexes.
// Inside it define an empty array, push the elements of arr at indexOne and indexTwo into it.
// Make it a string
// Return it