const arr = ['green','red','black','blue','brown','yellow','purple']
const isThere = (arr, ele)=> arr.indexOf(ele) != -1
module.exports ={
    arr, isThere
}
// == HINT === 
// Array.prototype.indexOf still rocks here...!
// == STEPS ===
// Create an array called arr as shown in the example.
// Define a function called isThere which takes 2 arguments, an array and a string or number.
// Using aArray.prototype.indexOf or Array.prototype.includes check if is there, and return true if it is and false if it is not.
