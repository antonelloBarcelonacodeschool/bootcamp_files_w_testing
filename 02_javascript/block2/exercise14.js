const isArrayFunc = arr => Array.isArray(arr)

// // no built-in methods
// let isArrayFunc = (arr) => {
// 	let origL = arr.length
// 	try{ 
// 		arr.push(1) 
// 	}
// 	catch(e){ 
// 		console.log(e)
// 		window.location.href = `https://stackoverflow.com/search?q=javascript ${e.message}`
// 	}
// 	finally{
// 		return arr.length != origL ? `modified` : `nope`}
// 	}

	module.exports = {
		isArrayFunc
	}
// == HINT === 
// Look into Array.isArray
// == STEPS ===
// Declare a function called isArrayFunc which takes a single argument, an array.
// Use Array.isArray to evaluate if the argument passed is indeed and array
// return true if it is
// return false if it's not.
