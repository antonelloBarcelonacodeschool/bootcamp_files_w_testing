const arr   = ['milk','cheese','car','lime'], index = 2; 
const assigner = (arr, index)=>[arr[index]];
module.exports = { arr, assigner, index };

// == HINT === 
// Use square brackets notation

// == STEPS ===
// Create a variable called arr and give it the values from the example
// Then create a variable called index and give it a value of 2.
// Create a function called assigner which takes the two variables we have defined as arguments.
// Take the element at the given index from the passed array using square brackets notation and return it.